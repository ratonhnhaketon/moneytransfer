package com.mt.app.server

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.mt.app.model.request.{AccountFeatureEnableRequest, AccountTypeUpdateRequest}
import com.mt.app.service.UpdateAccountInformationService
import spray.json._

class AccountUpdateRoute(updateAccountInformationService: UpdateAccountInformationService) extends ServiceRoute {

  import com.mt.app.converter.ImplicitRequestResponseConverters._

  override def getRoute(): Route = {
    path("updateAccountType") {
      put {
        entity(as[AccountTypeUpdateRequest]) {
          request =>
            complete {
              val maybeResponse = updateAccountInformationService.updateAccountType(request)
              maybeResponse.fold(
                error => error.getMessage.toJson,
                _ => "Account Type Updated Successfuly".toJson
              )
            }
        }
      }
    } ~
      path("updateXCurrInfo") {
        put {
          entity(as[AccountFeatureEnableRequest]) {
            request =>
              complete {
                val maybeResponse = updateAccountInformationService.updateXCurrencyTransfer(request)
                maybeResponse.fold(
                  error => error.getMessage.toJson,
                  _ => "Account XCurrency Feature Updated Successfuly".toJson
                )
              }
          }
        }
      } ~
      path("updateAccountState") {
        put {
          entity(as[AccountFeatureEnableRequest]) {
            request =>
              complete {
                val maybeResponse = updateAccountInformationService.updateAccountState(request)
                maybeResponse.fold(
                  error => error.getMessage.toJson,
                  _ => "Account State Updated Successfuly".toJson
                )
              }
          }
        }
      } ~
      path("updateAccountMarkup") {
        put {
          entity(as[AccountFeatureEnableRequest]) {
            request =>
              complete {
                val maybeResponse = updateAccountInformationService.updateAccountMarkUp(request)
                maybeResponse.fold(
                  error => error.getMessage.toJson,
                  _ => "Account MarkUp Updated Successfuly".toJson
                )
              }
          }
        }
      }
  }
}
