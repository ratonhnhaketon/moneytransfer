package com.mt.app.server

import akka.http.scaladsl.server.Route

trait ServiceRoute {
  def getRoute(): Route
}
