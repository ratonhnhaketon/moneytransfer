package com.mt.app.server

import akka.http.scaladsl.server.Directives.{as, complete, entity, path, post}
import akka.http.scaladsl.server.Route
import com.mt.app.model.request.MoneyTransferRequest
import com.mt.app.service.MoneyTransferServiceImpl
import spray.json._

class MoneyTransferRoute(moneyTransferService: MoneyTransferServiceImpl) extends ServiceRoute {

  import com.mt.app.converter.ImplicitRequestResponseConverters._

  override def getRoute(): Route = {
    path("initiateTransfer") {
      post {
        entity(as[MoneyTransferRequest]) {
          request =>
            complete {
              val transferResult = moneyTransferService.transfer(request)
              transferResult.fold(
                error => error.getMessage.toJson,
                _ => "Transferred Successfully".toJson
              )
            }
        }
      }
    }
  }
}
