package com.mt.app.server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.mt.app.configuration.BeanConfiguration
import com.typesafe.config.ConfigFactory

import scala.io.StdIn

/**
  * Main class to fire up the application.
  * This will load the bean configuration and start listening for requests
  */
object Boot extends App {

  System.setProperty("logback.configurationFile", "logback.xml")

  implicit val system = ActorSystem("MoneyTransferSystem")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  private val config = ConfigFactory.load()
  private val beanConfiguration = new BeanConfiguration

  private val host = config.getString("http.host")
  private val port = config.getInt("http.port")

  import beanConfiguration._

  // Add new service route to this Seq to make it live
  private val serviceRoute = Seq(
    accountInformationRoute,
    accountUpdateRoute,
    moneyTransferRoute)
    .map(_.getRoute())
    .reduce(_ ~ _)

  //Server StartUp
  val bindingFuture = Http().bindAndHandle(
    serviceRoute,
    host,
    port
  )

  println(s"Listening for requests at $host:$port \n Press ENTER to shutdown the server")
  StdIn.readLine()

  //Server Shutdown
  bindingFuture.flatMap(_.unbind())
  system.terminate()
}