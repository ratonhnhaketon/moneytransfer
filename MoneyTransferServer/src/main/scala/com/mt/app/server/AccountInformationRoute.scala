package com.mt.app.server

import akka.http.scaladsl.server.Directives.{complete, get, parameter, path, pathPrefix, _}
import akka.http.scaladsl.server.Route
import com.mt.app.model.request.{GetAccountInformationRequest, GetAccountTransactionsRequest}
import com.mt.app.model.response.GetTransactionResponse
import com.mt.app.service.GetAccountInformationServiceImpl
import spray.json._

class AccountInformationRoute(getAccountInfoService: GetAccountInformationServiceImpl) extends ServiceRoute {

  import com.mt.app.converter.ImplicitRequestResponseConverters._

  override def getRoute(): Route = {
    pathPrefix("getAccount") {
      path("information") {
        get {
          parameter('accountNumber.as[String], 'requestId.as[String]) {
            (accountNumber, requestId) =>
              complete {
                val result = getAccountInfoService.getAccountInformation(GetAccountInformationRequest(requestId, accountNumber))
                result.fold(
                  error => error.getMessage.toJson,
                  account => account.toJson
                )
              }
          }
        }
      } ~
        path("allTransactions") {
          get {
            parameter('accountNumber.as[String], 'transactionCount.as[Int], 'requestId.as[String]) {
              (accountNumber, transactionCount, requestId) =>
                complete {
                  val result = getAccountInfoService.getLastNAllTransactions(GetAccountTransactionsRequest(requestId, accountNumber, transactionCount))
                  result.fold(
                    error => error.getMessage.toJson,
                    transactions => GetTransactionResponse(transactions).toJson
                  )
                }
            }
          }
        } ~
        path("balance") {
          get {
            parameter('accountNumber.as[String], 'requestId.as[String]) {
              (accountNumber, requestId) =>
                complete {
                  val result = getAccountInfoService.getBalance(GetAccountInformationRequest(requestId, accountNumber))
                  result.fold(
                    error => error.getMessage.toJson,
                    accountBalance => accountBalance.toJson
                  )
                }
            }
          }
        } ~
        path("creditTransactions") {
          get {
            parameter('accountNumber.as[String], 'transactionCount.as[Int], 'requestId.as[String]) {
              (accountNumber, transactionCount, requestId) =>
                complete {
                  val result = getAccountInfoService.getLastNCreditTransactions(GetAccountTransactionsRequest(requestId, accountNumber, transactionCount))
                  result.fold(
                    error => error.getMessage.toJson,
                    creditTransactions => GetTransactionResponse(creditTransactions).toJson
                  )
                }
            }
          }
        } ~
        path("debitTransactions") {
          get {
            parameter('accountNumber.as[String], 'transactionCount.as[Int], 'requestId.as[String]) {
              (accountNumber, transactionCount, requestId) =>
                complete {
                  val result = getAccountInfoService.getLastNDebitTransactions(GetAccountTransactionsRequest(requestId, accountNumber, transactionCount))
                  result.fold(
                    error => error.getMessage.toJson,
                    creditTransactions => GetTransactionResponse(creditTransactions).toJson
                  )
                }
            }
          }
        }
    }
  }
}