package com.mt.app.configuration

import com.mt.app.lock.DummyDistributedLock
import com.mt.app.server.{AccountInformationRoute, AccountUpdateRoute, MoneyTransferRoute}
import com.mt.app.service.{GetAccountInformationServiceImpl, MoneyTransferServiceImpl, UpdateAccountInformationServiceImpl}
import com.mt.app.store.{AccountStore, ExchangeRateStore, TransactionStore}

/**
  * Dummy Bean Configuration creates all the beans on application Startup
  * Ths will be replaced by Google Guice or Spring IoC
  */
class BeanConfiguration {

  // should be injected from property file or config service
  private val markUp = 0.01
  private val bankAccountNumber = "3337771212"
  
  val dummyDistributedLock = new DummyDistributedLock[String]
  val accountStore: AccountStore = new AccountStore
  val transactionStore: TransactionStore = new TransactionStore
  val exchangeRate: ExchangeRateStore = new ExchangeRateStore

  val getAccountInformationService = new GetAccountInformationServiceImpl(accountStore, transactionStore)
  val updateAccountInformationService = new UpdateAccountInformationServiceImpl(accountStore, dummyDistributedLock)
  val moneyTransferService = new MoneyTransferServiceImpl(accountStore, exchangeRate, transactionStore, dummyDistributedLock, markUp, bankAccountNumber)

  val accountInformationRoute = new AccountInformationRoute(getAccountInformationService)
  val accountUpdateRoute = new AccountUpdateRoute(updateAccountInformationService)
  val moneyTransferRoute = new MoneyTransferRoute(moneyTransferService)
}
