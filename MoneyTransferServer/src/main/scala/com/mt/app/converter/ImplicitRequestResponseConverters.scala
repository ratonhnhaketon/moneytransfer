package com.mt.app.converter

import java.time.LocalDate

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.mt.app.model._
import com.mt.app.model.enum.AccountType.AccountType
import com.mt.app.model.enum.Currency.Currency
import com.mt.app.model.enum.TransactionType.TransactionType
import com.mt.app.model.enum.{AccountType, Currency, TransactionType}
import com.mt.app.model.request._
import com.mt.app.model.response.GetTransactionResponse
import spray.json.{DefaultJsonProtocol, JsString, JsValue, RootJsonFormat}

object ImplicitRequestResponseConverters extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val LocalDateFormat = new RootJsonFormat[LocalDate] {
    override def read(json: JsValue): LocalDate = LocalDate.parse(json.compactPrint)

    override def write(obj: LocalDate): JsValue = JsString(obj.toString)
  }

  implicit val CurrencyFormat = new RootJsonFormat[Currency] {
    override def read(json: JsValue): Currency = Currency.withName(json.compactPrint.toUpperCase)

    override def write(obj: Currency): JsValue = JsString(obj.toString)
  }

  implicit val AccTypeFormat = new RootJsonFormat[AccountType] {
    override def read(json: JsValue): AccountType = AccountType.withName(json.compactPrint.toUpperCase)

    override def write(obj: AccountType): JsValue = JsString(obj.toString)
  }

  implicit val TransactionFormat = new RootJsonFormat[TransactionType] {
    override def read(json: JsValue): TransactionType = TransactionType.withName(json.compactPrint.toUpperCase)

    override def write(obj: TransactionType): JsValue = JsString(obj.toString)
  }

  implicit val transactionFormat: RootJsonFormat[Transaction] = jsonFormat12(Transaction)
  implicit val accountFormat: RootJsonFormat[Account] = jsonFormat7(Account)
  implicit val transactionResponseFormat: RootJsonFormat[GetTransactionResponse] = jsonFormat1(GetTransactionResponse)
  implicit val accountTypeUpdateRequestFormat: RootJsonFormat[AccountTypeUpdateRequest] = jsonFormat3(AccountTypeUpdateRequest)
  implicit val accountFeatureEnableRequestFormat: RootJsonFormat[AccountFeatureEnableRequest] = jsonFormat3(AccountFeatureEnableRequest)
  implicit val moneyTransferRequestFormat: RootJsonFormat[MoneyTransferRequest] = jsonFormat5(MoneyTransferRequest)
  implicit val getAccountInformationRequest: RootJsonFormat[GetAccountInformationRequest] = jsonFormat2(GetAccountInformationRequest)
  implicit val getAccountTransactionRequest: RootJsonFormat[GetAccountTransactionsRequest] = jsonFormat3(GetAccountTransactionsRequest)
}
