package com.mt.app.model.enum

object Currency extends Enumeration {
  type Currency = Value
  val USD, GBP, INR = Value
}
