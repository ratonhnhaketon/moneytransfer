package com.mt.app.model

import com.mt.app.model.enum.AccountType.AccountType
import com.mt.app.model.enum.Currency.Currency

/**
  * Account case class that holds all the account level information
  *
  * @param accountNumber
  * @param balance
  * @param currency
  * @param accountType
  * @param xCurrencyTransferEnabled
  * @param isMarkupApplicable
  * @param isAccountFrozen
  */
case class Account(
                    accountNumber: String,
                    balance: BigDecimal,
                    currency: Currency,
                    accountType: AccountType,
                    xCurrencyTransferEnabled: Boolean = true,
                    isMarkupApplicable: Boolean = false,
                    isAccountFrozen: Boolean = false
                  )
