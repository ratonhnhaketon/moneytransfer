package com.mt.app.model.request

case class MoneyTransferRequest(
                                 requestId: String,
                                 sourceAccountNumber: String,
                                 targetAccountNumber: String,
                                 amount: BigDecimal,
                                 maybeRemarks: Option[String] = None
                               )