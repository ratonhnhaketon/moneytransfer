package com.mt.app.model.response

import com.mt.app.model.Transaction

case class GetTransactionResponse(transactions: Seq[Transaction])
