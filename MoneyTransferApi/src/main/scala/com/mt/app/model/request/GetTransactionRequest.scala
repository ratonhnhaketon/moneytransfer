package com.mt.app.model.request

case class GetTransactionRequest(accountNumber: String, transactionCount: Int)
