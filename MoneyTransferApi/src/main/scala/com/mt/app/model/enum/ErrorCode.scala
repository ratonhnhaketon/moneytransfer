package com.mt.app.model.enum

object ErrorCode extends Enumeration {

  type ErrorCode = Value
  val NO_ACCOUNT_ERROR = Value(100, "Account/s doesn't exist")
  val INVALID_ACCOUNT_ERROR = Value(101, "Invalid Account number. Account number is a numeric string with length 10")
  val NO_SOURCE_ACCOUNT_ERROR = Value(102, "Source Account doesn't exist")
  val NO_TARGET_ACCOUNT_ERROR = Value(103, "Target Account doesn't exist")
  val INVALID_TRANSACTION_COUNT_ERROR = Value(104, "Invalid Transaction Count. Count should be >0 and <10")

  val INSUFFICIENT_BALANCE_ERROR = Value(200, "Account doesn't have sufficient balance for transfer")
  val XCURR_TRANSFER_ERROR = Value(201, "Account is not enabled for X-Currency Transfer")
  val SOURCE_ACCOUNT_FREEZE_ERROR = Value(202, "Source Account is Frozen. Can't not process Money Transfer")
  val TARGET_ACCOUNT_FREEZE_ERROR = Value(203, "Target Account is Frozen. Can't not process Money Transfer")

  val TRANSACTION_IN_PROGRESS_ERROR = Value(301, "Transaction in progress for Source Account")
  val UPDATION_IN_PROGRESS_ERROR = Value(302, "Update is in progress for Source Account")
  val INVALID_TRANSFER_AMOUNT_ERROR = Value(303, "Invalid Amount. Amount must be >0 and <2000000")

  val EXCHANGE_RATE_ERROR = Value(402, "Exchange Rate is not available for the currency pair")
}
