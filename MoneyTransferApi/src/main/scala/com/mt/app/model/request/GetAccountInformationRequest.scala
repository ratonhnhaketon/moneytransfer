package com.mt.app.model.request

case class GetAccountInformationRequest(requestId: String, accountNumber: String)
