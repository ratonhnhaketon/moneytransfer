package com.mt.app.model.request

import com.mt.app.model.enum.AccountType.AccountType

case class AccountFeatureEnableRequest(requestId: String, accountNumber: String, isFeatureEnable: Boolean)

case class AccountTypeUpdateRequest(requestId: String, accountNumber: String, accountType: AccountType)
