package com.mt.app.model.enum

object TransactionType extends Enumeration {
  type TransactionType = Value
  val DEBIT, CREDIT = Value
}
