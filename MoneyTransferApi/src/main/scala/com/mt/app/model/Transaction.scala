package com.mt.app.model

import java.time.LocalDate
import java.util.UUID

import com.mt.app.model.enum.Currency.Currency
import com.mt.app.model.enum.TransactionType.TransactionType

/**
  *
  * @param transactionId
  * @param transactionOwner
  * @param sourceAccountNumber
  * @param targetAccountNumber
  * @param transactionDate
  * @param amount
  * @param startBalance
  * @param closingBalance
  * @param sourceCurrency
  */
case class Transaction(transactionId: String,
                       transactionOwner: String,
                       sourceAccountNumber: String,
                       targetAccountNumber: String,
                       transactionDate: LocalDate,
                       amount: BigDecimal,
                       startBalance: BigDecimal,
                       closingBalance: BigDecimal,
                       sourceCurrency: Currency,
                       targetCurrency: Currency,
                       transactionType: TransactionType,
                       remarks: Option[String] = None
                      ) extends Ordered[Transaction] {
  override def compare(that: Transaction): Int = {
    if (this.transactionDate.isAfter(that.transactionDate)) {
      -1
    } else if (this.transactionDate.isBefore(that.transactionDate)) {
      1
    } else {
      0
    }
  }
}
