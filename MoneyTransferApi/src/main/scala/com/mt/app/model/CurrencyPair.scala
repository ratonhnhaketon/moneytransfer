package com.mt.app.model

import com.mt.app.model.enum.Currency.Currency

case class CurrencyPair(sourceCurrency: Currency, targetCurrency: Currency)
