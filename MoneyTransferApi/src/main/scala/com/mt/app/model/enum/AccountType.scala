package com.mt.app.model.enum

object AccountType extends Enumeration {
  type AccountType = Value
  val REGULAR = Value("REGULAR")
  val PREMIUM = Value("PREMIUM")
}
