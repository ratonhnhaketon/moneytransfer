package com.mt.app.model

case class TransferInstruction(
                                requestId: String,
                                sourceAccount: Account,
                                targetAccount: Account,
                                transferAmount: BigDecimal,
                                markupCharge: BigDecimal = 0,
                                isXCurrencyTransfer: Boolean = false,
                                maybeApplicableExchangeRate: Option[BigDecimal] = None,
                                maybeRemarks: Option[String]
                              )
