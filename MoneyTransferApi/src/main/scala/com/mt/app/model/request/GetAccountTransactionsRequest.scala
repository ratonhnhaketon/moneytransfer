package com.mt.app.model.request

case class GetAccountTransactionsRequest(requestId: String, accountNumber: String, transactionCount: Int)
