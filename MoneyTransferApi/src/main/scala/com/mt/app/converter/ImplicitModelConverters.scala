package com.mt.app.converter

import scala.util.{Failure, Success}

/**
  * Implicit Converters to converter anyType to Try.Success or Try.Failure
  */
object ImplicitModelConverters {

  implicit class TryConversionOps[T <: AnyRef](val anyRef: T) extends AnyVal {
    def success = Success(anyRef)
    def failure = Failure(new Exception(anyRef.toString))
  }

}
