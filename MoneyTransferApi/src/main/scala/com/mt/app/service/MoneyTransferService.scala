package com.mt.app.service

import com.mt.app.model.request.MoneyTransferRequest

import scala.util.Try

trait MoneyTransferService {

  def transfer(transferRequest: MoneyTransferRequest): Try[Unit]

}
