package com.mt.app.service

import com.mt.app.model.request.{GetAccountInformationRequest, GetAccountTransactionsRequest}
import com.mt.app.model.{Account, Transaction}

import scala.util.Try

trait GetAccountInformationService {

  def getAccountInformation(getAccountInformationRequest: GetAccountInformationRequest): Try[Account]

  def getBalance(getAccountInformationRequest: GetAccountInformationRequest): Try[BigDecimal]

  def getLastNAllTransactions(getAccountTransactionsRequest: GetAccountTransactionsRequest): Try[Seq[Transaction]]

  def getLastNDebitTransactions(getAccountTransactionsRequest: GetAccountTransactionsRequest): Try[Seq[Transaction]]

  def getLastNCreditTransactions(getAccountTransactionsRequest: GetAccountTransactionsRequest): Try[Seq[Transaction]]
}
