package com.mt.app.service

import com.mt.app.model.request.{AccountFeatureEnableRequest, AccountTypeUpdateRequest}

import scala.util.Try

trait UpdateAccountInformationService {

  def updateAccountType(accountTypeUpdateRequest: AccountTypeUpdateRequest): Try[Unit]

  def updateXCurrencyTransfer(accountFeatureEnableRequest: AccountFeatureEnableRequest): Try[Unit]

  def updateAccountState(accountFeatureEnableRequest: AccountFeatureEnableRequest): Try[Unit]

  def updateAccountMarkUp(accountFeatureEnableRequest: AccountFeatureEnableRequest): Try[Unit]

}
