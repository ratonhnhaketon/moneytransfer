

# MoneyTransfer

**Money Transfer REST API**

**Features**

   -  Cross Currency Transfer

   -  Distributed Lock (Dummy for now, can use Hazelcast or other suitable distributed platform to achieve the locking) to support locking across the process instance

   -  Configurable markup information

   -  Ability to freeze/unfreeze account

   -  Ability to enable/disable X-currency transfer

   -  Ability to enable/disable markup for at level


**Testing**

   -  Unit tests cover all the possible scenarios in the money transfer application

   -  Right click and Run test package under MoneyTransferImpl>src

   -  Use PostMan/REST Sender for POST/PUT methods or a normal web browser to hit GET end points

**How to run**

   -  To fire up the server, go to Boot.scala file under /src/scala/com/mt/app/server in MoneyTransferServer module.

   -  Right click and run the main method, this should bring up the server

   -  Use below(at the bottom) URLs from browser/postman/rest sender to hit the service end points


**Scope of Improvement [In Next Release]**

   -  Currently all the GetAccountInformation calls are plain GET calls to test the REST API. It's not safe to use account number as a query param.
      Easy solution is to encrypt parameters before sending them over the network.

   -  Request Authentication and authorization. Since there's no authorization in place right now, I haven't added the user level information in the request. For example- user/customerId.
      User/Customer Id can be used for authorization purpose.

   -  Transaction Management while all the UpSert operations. You never know when things can go wrong.

   -  Currently the process is single threaded as it didn't make much sense to have multiple threads to process basic operation. However for a better throughput
      it can be enhanced to support multithreaded processing (Can use Akka-Actors to achieve concurrent processing)

   -  Currently I've not used any framework for DI. (Can use Google Guice or Spring IoC)




**1_**
http://localhost:9000/getAccount/allTransactions?accountNumber=1111111111&transactionCount=2&requestId=sshdiooi67-asdd
http://localhost:9000/getAccount/information?accountNumber=1111111111&requestId=sshdiooi67-asdd

**(Set content type application/json in headers)**

**2_**
http://localhost:9000/updateXCurrInfo

    {
        "requestId": "sshdiooi67-asdd",
        "accountNumber": "1111111111",
        "isFeatureEnable": true
    }


**3_**

    http://localhost:9000/initiateTransfer
    {
        "requestId": "sshdiooi67-asdd",
        "sourceAccountNumber": "2222222222",
        "targetAccountNumber": "1111111111",
        "amount": 1200,
        "remarks": "House Rent"
    }