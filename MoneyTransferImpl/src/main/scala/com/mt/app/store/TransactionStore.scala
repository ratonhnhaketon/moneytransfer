package com.mt.app.store

import java.time.LocalDate

import com.mt.app.model.Transaction
import com.mt.app.model.enum.Currency._
import com.mt.app.model.enum.TransactionType._

import scala.collection.mutable

/**
  * Dummy In Memory Transaction Store. Ideally this data should go in the RDBMS
  * and Transaction should be maintained, so that it can be rolled-back if things go south.
  */
class TransactionStore {

  private var transactionRecords: Seq[Transaction] = mutable.Seq(
    Transaction("6d00ede2-c9cf-4d8c-a915-012a717155ef", "1111111111", "1111111111", "2222222222", LocalDate.of(2018, 4, 1), 1000, 90000, 89000, USD, INR, DEBIT),
    Transaction("7tf00edd-csdt-fd87-d9ff-9sf999f09990", "1111111111", "3333333333", "1111111111", LocalDate.of(2018, 4, 3), 5000, 89000, 94000, USD, INR, CREDIT),
    Transaction("6d00ede2-c9cf-4d8c-fh00-9updf999bxli", "2222222222", "1111111111", "2222222222", LocalDate.of(2018, 4, 1), 1000, 90000, 91000, USD, INR, CREDIT)
  )

  def getLastNCreditTransactions(accountNumber: String, numberOfTransactions: Int): Seq[Transaction] =
    getLastNTransactions(accountNumber, numberOfTransactions, transaction => transaction.transactionType.equals(CREDIT))

  private def getLastNTransactions(accountNumber: String, numberOfTransactions: Int, filterFun: Transaction => Boolean): Seq[Transaction] =
    transactionRecords.toStream.filter(transaction => transaction.transactionOwner.equals(accountNumber) && filterFun(transaction))
      .sorted
      .take(numberOfTransactions)


  def getLastNAllTransactions(accountNumber: String, numberOfTransaction: Int): Seq[Transaction] = getLastNTransactions(accountNumber, numberOfTransaction, _ => true)

  def getLastNDebitTransactions(accountNumber: String, numberOfTransactions: Int): Seq[Transaction] =
    getLastNTransactions(accountNumber, numberOfTransactions, transaction => transaction.transactionType.equals(DEBIT))

  def persistTransaction(transaction: Transaction): Unit = transactionRecords = transactionRecords :+ transaction

  def persistTransactions(transactions: Seq[Transaction]): Unit = transactionRecords = transactionRecords ++ transactions

}
