package com.mt.app.store

import java.util.concurrent.ConcurrentHashMap

import com.mt.app.model.CurrencyPair
import com.mt.app.model.enum.Currency.{Currency, _}

/**
  * Dummy In Memory Exchange Rate Store. Ideally this data should go in the RDBMS or I can hook to some other,
  * service that provides live exchange rate. Depends on the type of resource availability and requirement.
  */
class ExchangeRateStore {
  private val exchangeRateMap: ConcurrentHashMap[CurrencyPair, BigDecimal] = new ConcurrentHashMap()

  exchangeRateMap.put(CurrencyPair(USD, INR), 0.06)
  exchangeRateMap.put(CurrencyPair(INR, USD), 65.21)
  exchangeRateMap.put(CurrencyPair(USD, GBP), 0.70)
  exchangeRateMap.put(CurrencyPair(GBP, USD), 1.42)
  exchangeRateMap.put(CurrencyPair(INR, GBP), 0.01)
  exchangeRateMap.put(CurrencyPair(GBP, INR), 92.87)

  def getExchangeRateFor(sourceCurrency: Currency, targetCurrency: Currency): Option[BigDecimal] =
    if (exchangeRateMap.containsKey(CurrencyPair(sourceCurrency, targetCurrency)))
      Some(exchangeRateMap.get(CurrencyPair(sourceCurrency, targetCurrency)))
    else
      None

  def updateExchangeRate(sourceCurrency: Currency, targetCurrency: Currency, newExRate: BigDecimal): BigDecimal =
    exchangeRateMap.put(CurrencyPair(sourceCurrency, targetCurrency), newExRate)
}
