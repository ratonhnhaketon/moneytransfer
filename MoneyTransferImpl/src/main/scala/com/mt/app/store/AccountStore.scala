package com.mt.app.store

import java.util.concurrent.ConcurrentHashMap

import com.mt.app.converter.ImplicitModelConverters._
import com.mt.app.model.Account
import com.mt.app.model.enum.AccountType.{AccountType, _}
import com.mt.app.model.enum.Currency._
import com.mt.app.model.enum.ErrorCode._

import scala.util.{Failure, Success, Try}

/**
  * Dummy In Memory Account Date Store. Ideally this data should go in the RDBMS
  * and Transaction should be maintained, so that it can be rolled-back if things go south.
  */
class AccountStore {

  private val accountMap: ConcurrentHashMap[String, Account] = new ConcurrentHashMap

  accountMap.put("1111111111", Account("1111111111", 80000, USD, REGULAR, xCurrencyTransferEnabled = false))
  accountMap.put("2222222222", Account("2222222222", 3500000, INR, PREMIUM, isMarkupApplicable = true))
  accountMap.put("3333333333", Account("3333333333", 140000, GBP, REGULAR, isAccountFrozen = true))
  accountMap.put("4444444444", Account("4444444444", 90000, INR, REGULAR))
  accountMap.put("5555555555", Account("5555555555", 100000, GBP, PREMIUM))
  accountMap.put("6666666666", Account("6666666666", 700000, USD, PREMIUM))
  accountMap.put("7777777777", Account("7777777777", 500000, GBP, REGULAR))
  accountMap.put("8888888888", Account("8888888888", 1000000, USD, PREMIUM))

  def updateAccountBalance(accountNumber: String, newBalance: BigDecimal): Try[Unit] = {
    baseAccountCheck(accountNumber, newBalance) {
      (_accountNumber, _newBalance) =>
        accountMap.put(_accountNumber, accountMap.get(_accountNumber).copy(balance = _newBalance))
    }
  }

  def updateAccountType(accountNumber: String, newAccountType: AccountType): Try[Unit] = {
    baseAccountCheck(accountNumber, newAccountType) {
      (_accountNumber, _accountType) =>
        val updatedAccount = accountMap.get(_accountNumber).copy(accountType = _accountType)
        accountMap.put(accountNumber, updatedAccount)
    }
  }

  def hasSufficientBalance(accountNumber: String, requiredBalance: BigDecimal): Try[Unit] =
    getAccountInformation(accountNumber) match {
      case Failure(ex) => Failure(ex)
      case Success(account) =>
        if (account.balance < requiredBalance)
          INSUFFICIENT_BALANCE_ERROR.failure
        else
          Success(Unit)
    }

  def getAccountInformation(accountNumber: String): Try[Account] =
    baseAccountCheck(accountNumber, null)((_accountNumber, _) => accountMap.get(_accountNumber))

  def getAccountsInformation(accountNumbers: Seq[String]): Map[String, Try[Account]] = accountNumbers.map(accountNum => accountNum -> getAccountInformation(accountNum)).toMap

  def updateXCurrencyTransactions(accountNumber: String, isEnable: Boolean): Try[Unit] =
    baseAccountCheck(accountNumber, isEnable) {
      (_accountNumber, _isEnable) =>
        val updatedAccount = accountMap.get(_accountNumber).copy(xCurrencyTransferEnabled = _isEnable)
        accountMap.put(_accountNumber, updatedAccount)
    }

  def updateAccountState(accountNumber: String, _shouldFreeze: Boolean): Try[Unit] =
    baseAccountCheck(accountNumber, _shouldFreeze) {
      (_accountNumber, _shouldFreeze) =>
        val updatedAccount = accountMap.get(_accountNumber).copy(isAccountFrozen = _shouldFreeze)
        accountMap.put(_accountNumber, updatedAccount)
    }

  private def baseAccountCheck[T, R](accountNumber: String, value: T)(f: (String, T) => R): Try[R] = {
    if (doesAccountExist(accountNumber)) {
      Success(f(accountNumber, value))
    } else {
      NO_ACCOUNT_ERROR.failure
    }
  }

  def doesAccountExist(accountNumber: String): Boolean = accountMap.containsKey(accountNumber)

  def updateAccountMarkupInfo(accountNumber: String, markupApplicability: Boolean): Try[Unit] =
    baseAccountCheck(accountNumber, markupApplicability) {
      (_accountNumber, _markupApplicability) =>
        val updatedAccount = accountMap.get(_accountNumber).copy(isMarkupApplicable = _markupApplicability)
        accountMap.put(_accountNumber, updatedAccount)
    }
}