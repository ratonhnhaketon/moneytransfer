package com.mt.app.validation

import com.mt.app.converter.ImplicitModelConverters._
import com.mt.app.model.TransferInstruction
import com.mt.app.model.enum.ErrorCode.{EXCHANGE_RATE_ERROR, NO_ACCOUNT_ERROR, NO_SOURCE_ACCOUNT_ERROR, NO_TARGET_ACCOUNT_ERROR, SOURCE_ACCOUNT_FREEZE_ERROR, TARGET_ACCOUNT_FREEZE_ERROR, XCURR_TRANSFER_ERROR}
import com.mt.app.model.request.MoneyTransferRequest
import com.mt.app.store.{AccountStore, ExchangeRateStore}
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success, Try}
trait TransferInstructionValidations extends LazyLogging{
  protected val _accountStore: AccountStore
  protected val _markUp: BigDecimal
  protected val _exchangeRate: ExchangeRateStore

  /**
    * This method checks for the existence of both source and target accounts.
    * Long list of CASE is to capture the exact pain point in case of issue.
    *
    * @param transferRequest
    * @return
    */
  protected def validateAccounts(transferRequest: MoneyTransferRequest): Try[TransferInstruction] = {
    logger.info(s"Validating accounts for - ${transferRequest.requestId}")

    val accountMap = _accountStore.getAccountsInformation(Seq(transferRequest.sourceAccountNumber, transferRequest.targetAccountNumber))
    val maybeSourceAccount = accountMap(transferRequest.sourceAccountNumber)
    val maybeTargetAccount = accountMap(transferRequest.targetAccountNumber)
    (maybeSourceAccount, maybeTargetAccount) match {
      case (Success(sourceAccount), Success(targetAccount)) =>
        TransferInstruction(transferRequest.requestId, sourceAccount, targetAccount, transferRequest.amount, transferRequest.amount, maybeRemarks = transferRequest.maybeRemarks).success
      case (Success(_), Failure(_)) =>
        NO_TARGET_ACCOUNT_ERROR.failure
      case (Failure(_), Success(_)) =>
        NO_SOURCE_ACCOUNT_ERROR.failure
      case (Failure(_), Failure(_)) =>
        NO_ACCOUNT_ERROR.failure
    }
  }

  /**
    * This method does some primary checks on source account and target account.
    * It makes sure that none of the accounts is frozen. Source account has X-
    * currencyTransfer flag on if the transaction involves X-currency conversion.
    *
    * @param maybeInstruction
    * @return
    */
  protected def checkAccountFeatures(maybeInstruction: Try[TransferInstruction]): Try[TransferInstruction] = {
    checkTransactionInstruction(maybeInstruction) {
      _instruction =>
        logger.info(s"Checking account features for - ${_instruction.requestId}")
        val (source, target) = (_instruction.sourceAccount, _instruction.targetAccount)
        if (source.isAccountFrozen) {
          SOURCE_ACCOUNT_FREEZE_ERROR.failure
        } else if (target.isAccountFrozen) {
          TARGET_ACCOUNT_FREEZE_ERROR.failure
        } else if (source.currency != target.currency && !source.xCurrencyTransferEnabled) {
          XCURR_TRANSFER_ERROR.failure
        } else if (source.currency != target.currency && source.xCurrencyTransferEnabled) {
          _instruction.copy(isXCurrencyTransfer = true).success
        } else {
          _instruction.success
        }
    }
  }

  /**
    * This method checks whether the markup is applicable for the given Money Transfer Instruction or not.
    * If yes then it applies the markup and the total amount deducted from source account will be slightly
    * (as per the markup %) higher than the requested amount.
    *
    * @param maybeInstruction
    * @return
    */
  protected def applyMarkupIfApplicable(maybeInstruction: Try[TransferInstruction]): Try[TransferInstruction] = {
    maybeInstruction.map {
      _instruction =>
        logger.info(s"Applying markup if required - ${_instruction.requestId}")
        if (_instruction.sourceAccount.isMarkupApplicable) {
          val markup = _instruction.transferAmount * _markUp
          _instruction.copy(markupCharge = markup)
        } else {
          _instruction
        }
    }
  }

  /**
    * This method adjusts the transfer amount as per the exchange rate(if applicable).
    * As of now I'm assuming that the amount entered by the user is the amount he/she wants
    * to credit in target account in their local currency so the final deduction from the
    * source account might be higher than requested. But again this behaviour can be altered
    * as per the requirement.
    *
    * @param maybeInstruction
    * @return
    */
  protected def applyExchangeRateIfAvailable(maybeInstruction: Try[TransferInstruction]): Try[TransferInstruction] = {
    checkTransactionInstruction(maybeInstruction) {
      _instruction =>
        logger.info(s"Applying exchange rate if required - ${_instruction.requestId}")
        if (_instruction.isXCurrencyTransfer) {
          _exchangeRate.getExchangeRateFor(_instruction.sourceAccount.currency, _instruction.targetAccount.currency) match {
            case Some(rate) =>
              val updatedAmount = _instruction.transferAmount * rate
              _instruction.copy(transferAmount = updatedAmount, maybeApplicableExchangeRate = Some(rate)).success
            case None =>
              EXCHANGE_RATE_ERROR.failure
          }
        } else {
          _instruction.success
        }
    }
  }

  /**
    * This method makes sure that sufficient balance is available after applying markup and exchange rate
    * (If applicable of course).
    *
    * @param maybeInstruction
    * @return
    */
  protected def checkRequiredBalance(maybeInstruction: Try[TransferInstruction]): Try[TransferInstruction] = {
    checkTransactionInstruction(maybeInstruction) {
      _instruction =>
        logger.info(s"Checking balance availability - ${_instruction.requestId}")
        _accountStore.hasSufficientBalance(_instruction.sourceAccount.accountNumber, _instruction.transferAmount) match {
          case Success(_) => _instruction.success
          case Failure(ex) => ex.failure
        }
    }
  }

  private def checkTransactionInstruction(instruction: Try[TransferInstruction])(fun: TransferInstruction => Try[TransferInstruction]): Try[TransferInstruction] = {
    instruction match {
      case Success(_instruction) => fun(_instruction)
      case error => error
    }
  }
}