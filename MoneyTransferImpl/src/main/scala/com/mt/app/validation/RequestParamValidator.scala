package com.mt.app.validation

object RequestParamValidator {

  def validateAccountNumber(accountNumber: String): Boolean =
    accountNumber.matches("[0-9]+") && accountNumber.length == 10

  def validateAccountNumbers(accountNumbers: String*): Boolean = accountNumbers.forall(validateAccountNumber)

  def validateTransactionCount(transactionCount: Int): Boolean = 0 < transactionCount && 10 > transactionCount

  def validateAmount(amount: BigDecimal): Boolean = 0 < amount && 2000000 > amount

}
