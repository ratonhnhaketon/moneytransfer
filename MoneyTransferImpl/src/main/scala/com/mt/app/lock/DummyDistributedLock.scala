package com.mt.app.lock

import java.util.concurrent.ConcurrentHashMap

/**
  * This is a dummy locking mechanism to have a distributed lock like functionality.
  * All the threads trying to DEBIT or CREDIT to a particular account will be blocked if,
  * someone else has already acquired the lock on that account.
  * Ideally this responsibility will be taken care by Hazelcast or other distributed locking/caching platform in real world.
  * @tparam T
  */
class DummyDistributedLock[T] {

  private val locks = new ConcurrentHashMap[T, Object]()

  def tryLock(key: T): Boolean = {
    if (locks.containsKey(key)) {
      false
    } else {
      locks.put(key, new Object)
      true
    }
  }

  /**
    * Acquires locks if all the locks are available
    * @param keys
    * @return
    */
  def tryLock(keys: T*): Boolean = {
    if (keys.forall(key => !locks.containsKey(key))) {
      keys.foreach(locks.put(_, new Object))
      true
    } else {
      false
    }
  }

  def unLock(key: T): Boolean = {
    if (locks.containsKey(key)) {
      locks.remove(key)
      true
    } else {
      false
    }
  }

  /**
    * Releases all the locks if all the locks are currently owned by the thread
    * @param keys
    * @return
    */
  def unLock(keys: T*): Boolean = {
    if (keys.forall(locks.containsKey)) {
      keys.foreach(locks.remove)
      true
    } else {
      false
    }
  }

  def isLockAcquired(key: T): Boolean = locks.containsKey(key)

  def clear(): Unit = locks.clear()
}
