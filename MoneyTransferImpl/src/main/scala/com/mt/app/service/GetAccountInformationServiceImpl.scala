package com.mt.app.service

import com.mt.app.converter.ImplicitModelConverters._
import com.mt.app.model.enum.ErrorCode._
import com.mt.app.model.request.{GetAccountInformationRequest, GetAccountTransactionsRequest}
import com.mt.app.model.{Account, Transaction}
import com.mt.app.store.{AccountStore, TransactionStore}
import com.mt.app.validation.RequestParamValidator._
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

/**
  * Service Implementation for all the Get Account/Transaction info calls.
  *
  * @param accountStore
  * @param transactionStore
  */
class GetAccountInformationServiceImpl(accountStore: AccountStore, transactionStore: TransactionStore) extends GetAccountInformationService with LazyLogging {

  override def getBalance(request: GetAccountInformationRequest): Try[BigDecimal] = getAccountInformation(request).map(_.balance)

  override def getAccountInformation(request: GetAccountInformationRequest): Try[Account] =
    if (validateAccountNumber(request.accountNumber)) {
      logger.info(s"Fetching account details for ${request.accountNumber}")
      accountStore.getAccountInformation(request.accountNumber)
    }
    else {
      logger.info(s"Validation failed for ${request.accountNumber}")
      INVALID_ACCOUNT_ERROR.failure
    }

  override def getLastNAllTransactions(request: GetAccountTransactionsRequest): Try[Seq[Transaction]] = getLastNTransactions(request, transactionStore.getLastNAllTransactions)

  override def getLastNDebitTransactions(request: GetAccountTransactionsRequest): Try[Seq[Transaction]] = getLastNTransactions(request, transactionStore.getLastNDebitTransactions)

  /**
    * This generic method takes a function as one of the arguments.
    * Caller of this function will provide the behaviour.
    *
    * @param request
    * @param getTransactions
    * @return
    */
  private def getLastNTransactions(request: GetAccountTransactionsRequest, getTransactions: (String, Int) => Seq[Transaction]): Try[Seq[Transaction]] = {
    if (!validateAccountNumber(request.accountNumber)) {
      logger.error(s"Account Validation check failed for $request.accountNumber")
      INVALID_ACCOUNT_ERROR.failure
    }
    else if (!validateTransactionCount(request.transactionCount)) {
      logger.error(s"Invalid Transaction Count for $request.transactionCount")
      INVALID_TRANSACTION_COUNT_ERROR.failure
    }
    else if (!accountStore.doesAccountExist(request.accountNumber)) {
      logger.error(s"Couldn't find account for $request.accountNumber")
      NO_ACCOUNT_ERROR.failure
    }
    else {
      logger.info(s"Fetching last ${request.transactionCount} transaction/s for ${request.accountNumber}")
      Try(getTransactions(request.accountNumber, request.transactionCount))
    }
  }

  override def getLastNCreditTransactions(request: GetAccountTransactionsRequest): Try[Seq[Transaction]] = getLastNTransactions(request, transactionStore.getLastNCreditTransactions)
}
