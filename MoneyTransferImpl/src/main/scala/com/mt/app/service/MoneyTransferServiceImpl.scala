package com.mt.app.service

import java.time.LocalDate
import java.util.UUID

import com.mt.app.converter.ImplicitModelConverters._
import com.mt.app.lock.DummyDistributedLock
import com.mt.app.model.enum.ErrorCode._
import com.mt.app.model.enum.TransactionType._
import com.mt.app.model.request.MoneyTransferRequest
import com.mt.app.model.{Account, Transaction, TransferInstruction}
import com.mt.app.store.{AccountStore, ExchangeRateStore, TransactionStore}
import com.mt.app.validation.RequestParamValidator._
import com.mt.app.validation.TransferInstructionValidations

import scala.util.{Failure, Success, Try}

/**
  * Service Implementation for the MoneyTransfer Request
  *
  * @param accountStore
  * @param exchangeRate
  * @param transactionStore
  * @param lock
  * @param markUp
  */
class MoneyTransferServiceImpl(
                                accountStore: AccountStore,
                                exchangeRate: ExchangeRateStore,
                                transactionStore: TransactionStore,
                                lock: DummyDistributedLock[String],
                                markUp: BigDecimal,
                                bankAccountNumber: String,
                              ) extends MoneyTransferService with TransferInstructionValidations {

  override protected val _accountStore: AccountStore = accountStore
  override protected val _markUp: BigDecimal = markUp
  override protected val _exchangeRate: ExchangeRateStore = exchangeRate

  import MoneyTransferServiceImpl._

  /**
    * This method does some basic validation checks and delegates responsibility to the other method to further process
    * Money Transfer if everything goes well.
    * It also acquires the lock before initiating the transfer and releases the locks on complete. All the Exceptions
    * will be captured in Try so we won't run into a situation where we end up locking accounts forever.
    *
    * @param request
    * @return
    */
  override def transfer(request: MoneyTransferRequest): Try[Unit] = {
    if (!validateAccountNumbers(request.sourceAccountNumber, request.targetAccountNumber)) {
      logger.error(s"Invalid account Number. Source- ${request.sourceAccountNumber}, target- ${request.targetAccountNumber}")
      INVALID_ACCOUNT_ERROR.failure
    }
    else if (!validateAmount(request.amount)) {
      logger.error(s"Invalid amount. Amount- ${request.amount}")
      INVALID_TRANSFER_AMOUNT_ERROR.failure
    }
    else {
      if (lock.tryLock(request.sourceAccountNumber, request.targetAccountNumber)) {
        logger.debug(s"Acquired distributed lock on ${request.sourceAccountNumber} and ${request.targetAccountNumber}")
        val result = initiateTransfer(request)
        logger.debug(s"Releasing distributed lock on ${request.sourceAccountNumber} and ${request.targetAccountNumber}")
        lock.unLock(request.sourceAccountNumber, request.targetAccountNumber)
        result
      }
      else {
        logger.error(s"Transaction already in progress for one of the accounts. Request- ${request.requestId} aborted")
        TRANSACTION_IN_PROGRESS_ERROR.failure
      }
    }
  }

  /**
    * This method is kind of an orchestrator for method that does validation checks and transfer instruction building
    * task. On success, it persists the transactions and updated account balance in the in memory data.
    * No transactions are being maintained while doing the database persistence. In real world, there has to be transactions
    * to handle the UpSerts.
    *
    * @param transferRequest
    * @return
    */
  private def initiateTransfer(transferRequest: MoneyTransferRequest): Try[Unit] = {

    val validatedInstruction = validateAccounts(transferRequest)
      .customFold(checkAccountFeatures)
      .customFold(applyMarkupIfApplicable)
      .customFold(applyExchangeRateIfAvailable)
      .customFold(checkRequiredBalance)

    validatedInstruction.map {
      _instruction =>
        val (transactions, sourceAccount, targetAccount) = calculateAndBuildTransactions(_instruction)

        /*
        Below code should be executed in one DB transaction in real world. Since there's no DB involved in this
        I've not managed the Commit & Rollback of transaction in Good and Bad situation respectively.
         */
        logger.debug(s"Updating closing balance for source and target account for ${_instruction.requestId}")
        accountStore.updateAccountBalance(sourceAccount.accountNumber, sourceAccount.balance)
        accountStore.updateAccountBalance(targetAccount.accountNumber, targetAccount.balance)

        logger.debug(s"Persisting transactions for ${_instruction.requestId}")
        transactionStore.persistTransactions(transactions)
    }
  }

  /**
    * This method builds 2 transactions for one money transfer instruction.
    * One is Credit transaction for the target account and the other is Debit Transaction for
    * source account. Debit transaction will have adjusted transfer amount as per the exchange
    * rate if source and target currencies are different.
    *
    * @param instruction
    * @return
    */
  private def calculateAndBuildTransactions(instruction: TransferInstruction): (Seq[Transaction], Account, Account) = {
    logger.info(s"Building Transactions for instruction - ${instruction.requestId}")
    val (source, target) = (instruction.sourceAccount, instruction.targetAccount)
    val transactionId = UUID.randomUUID().toString

    // Exchange Rate will be set only when X-Currency Transfer is involved. It will be None otherwise
    val targetTransferAmt = instruction.maybeApplicableExchangeRate.map(instruction.transferAmount / _)
      .getOrElse(instruction.transferAmount)

    val sourceClosingBalance = source.balance - instruction.transferAmount - instruction.markupCharge
    val targetClosingBalance = target.balance + targetTransferAmt

    val debitTransaction = Transaction(transactionId, source.accountNumber, source.accountNumber, target.accountNumber, LocalDate.now(), instruction.transferAmount,
      source.balance, sourceClosingBalance, source.currency, target.currency, DEBIT)
    val creditTransaction = Transaction(transactionId, target.accountNumber, source.accountNumber, target.accountNumber, LocalDate.now(), targetTransferAmt,
      target.balance, targetClosingBalance, source.currency, target.currency, CREDIT)

    val transactions = if (source.isMarkupApplicable) {
      val markupTransaction = debitTransaction.copy(targetAccountNumber = bankAccountNumber, amount = instruction.markupCharge)
      Seq(debitTransaction, creditTransaction, markupTransaction)
    } else {
      Seq(debitTransaction, creditTransaction)
    }
    (transactions, source.copy(balance = sourceClosingBalance), target.copy(balance = targetClosingBalance))
  }
}

object MoneyTransferServiceImpl {

  /**
    * This is a custom implementation of a fold like isomorphism to take advantage of function pipeline
    * type of structure while applying validations and building the transfer instruction.
    * This can be implicitly applied on Try[TransferInstruction] type.
    *
    * @param maybeInstruction
    */
  implicit class CustomTryFold(val maybeInstruction: Try[TransferInstruction]) extends AnyVal {
    def customFold(func: Try[TransferInstruction] => Try[TransferInstruction]): Try[TransferInstruction] =
      maybeInstruction match {
        case Failure(ex) => Failure(ex)
        case Success(_) => func(maybeInstruction)
      }
  }
}
