package com.mt.app.service

import com.mt.app.lock.DummyDistributedLock
import com.mt.app.model.enum.ErrorCode._
import com.mt.app.model.request.{AccountFeatureEnableRequest, AccountTypeUpdateRequest}
import com.mt.app.store.AccountStore
import com.mt.app.validation.RequestParamValidator
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try
import com.mt.app.converter.ImplicitModelConverters._

/**
  * Service Implementation for all the Update Account Information calls
  *
  * @param accountStore
  * @param dummyDistributedLock
  */
class UpdateAccountInformationServiceImpl(accountStore: AccountStore, dummyDistributedLock: DummyDistributedLock[String]) extends UpdateAccountInformationService with LazyLogging {

  override def updateAccountType(request: AccountTypeUpdateRequest): Try[Unit] =
    validateAndTryLock(request.requestId, request.accountNumber, request.accountType) {
      (_accountNumber, _accountType) =>
        accountStore.updateAccountType(_accountNumber, _accountType)
    }

  override def updateXCurrencyTransfer(request: AccountFeatureEnableRequest): Try[Unit] =
    validateAndTryLock(request.requestId, request.accountNumber, request.isFeatureEnable) {
      (_accountNumber, _shouldEnableXTransfer) =>
        accountStore.updateXCurrencyTransactions(_accountNumber, _shouldEnableXTransfer)
    }

  /**
    * This is a generic method and takes function as one of the arguments.
    * This method first validates the account information and calls the call by name function if,
    * all the validations are green.
    *
    * @param accountNumber
    * @param updateVal
    * @param function
    * @tparam T
    * @return
    */
  private def validateAndTryLock[T](requestId: String, accountNumber: String, updateVal: T)(function: (String, T) => Try[Unit]): Try[Unit] = {
    if (RequestParamValidator.validateAccountNumber(accountNumber)) {
      if (dummyDistributedLock.tryLock(accountNumber)) {
        logger.debug(s"Acquired Lock on $accountNumber for Request- $requestId")

        logger.info(s"Updating Account Info for Request- $requestId, Account- $accountNumber")
        val result = function.apply(accountNumber, updateVal)

        logger.debug(s"Releasing Lock on $accountNumber for Request- $requestId")
        dummyDistributedLock.unLock(accountNumber)
        result
      }
      else {
        UPDATION_IN_PROGRESS_ERROR.failure
      }
    }
    else {
      INVALID_ACCOUNT_ERROR.failure
    }
  }

  override def updateAccountState(request: AccountFeatureEnableRequest): Try[Unit] =
    validateAndTryLock(request.requestId, request.accountNumber, request.isFeatureEnable) {
      (_accountNumber, _shouldFreeze) =>
        accountStore.updateAccountState(_accountNumber, _shouldFreeze)
    }

  override def updateAccountMarkUp(request: AccountFeatureEnableRequest): Try[Unit] = {
    validateAndTryLock(request.requestId, request.accountNumber, request.isFeatureEnable) {
      (_accountNumber, _shouldApplyMarkup) =>
        accountStore.updateAccountMarkupInfo(_accountNumber, _shouldApplyMarkup)
    }
  }
}
