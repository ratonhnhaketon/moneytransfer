package com.mt.app.validation

import com.mt.app.validation.RequestParamValidator._
import org.junit.runner.RunWith
import org.scalatest.Matchers.convertToAnyShouldWrapper
import org.scalatest.WordSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class RequestParamValidatorSpec extends WordSpec {

  "account having length!= 10 should be discarded" in {
    validateAccountNumber("12345678902") shouldBe false
    validateAccountNumber("223232") shouldBe false
  }

  "account having special characters and alphabets should be discarded" in {
    validateAccountNumber("B23456789A") shouldBe false
    validateAccountNumber("@123456789#") shouldBe false
  }

  "acount having numbers and length 10 should be accepted" in {
    validateAccountNumber("1234567890") shouldBe true
  }

  "Transaction count value >10 and <0 should be discarded" in {
    validateTransactionCount(11) shouldBe false
    validateTransactionCount(-1) shouldBe false
  }

  "Transaction count value <10 and >0 should be accepted" in {
    validateTransactionCount(2) shouldBe true
  }

  "Transfer count value < 1 and > 2,000,000 should be discarded" in {
    validateAmount(0) shouldBe false
    validateTransactionCount(2000001) shouldBe false
  }

  "Transfer count value > 0 and < 2,000,000 should be accepted" in {
    validateAmount(150000) shouldBe true
  }
}
