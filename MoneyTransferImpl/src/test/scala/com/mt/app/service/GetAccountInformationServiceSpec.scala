package com.mt.app.service

import java.time.LocalDate

import com.mt.app.converter.ImplicitModelConverters._
import com.mt.app.model.enum.AccountType.REGULAR
import com.mt.app.model.enum.Currency.{INR, USD}
import com.mt.app.model.enum.ErrorCode._
import com.mt.app.model.enum.TransactionType._
import com.mt.app.model.request.{GetAccountInformationRequest, GetAccountTransactionsRequest}
import com.mt.app.model.{Account, Transaction}
import com.mt.app.store.{AccountStore, TransactionStore}
import org.junit.runner.RunWith
import org.scalatest.Matchers.convertToAnyShouldWrapper
import org.scalatest.WordSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GetAccountInformationServiceSpec extends WordSpec {

  private val getAccountInfoService = new GetAccountInformationServiceImpl(new AccountStore, new TransactionStore)

  "getAccountInformation method should return error" when {
    "invoked with invalid account" in {
      assertFailure(GetAccountInformationRequest("srooor24450-sdkss", "WRONGACC1"), INVALID_ACCOUNT_ERROR, getAccountInfoService.getAccountInformation)
    }
    "invoked with nonexistent account" in {
      assertFailure(GetAccountInformationRequest("srooor24450-sdkss", "1234567890"), NO_ACCOUNT_ERROR, getAccountInfoService.getAccountInformation)
    }
  }

  "getBalance method should return error" when {
    "invoked with invalid account" in {
      assertFailure(GetAccountInformationRequest("srooor24450-sdkss", "WRONGACC1"), INVALID_ACCOUNT_ERROR, getAccountInfoService.getBalance)
    }
    "invoked with nonexistent account" in {
      assertFailure(GetAccountInformationRequest("srooor24450-sdkss", "1234567890"), NO_ACCOUNT_ERROR, getAccountInfoService.getBalance)
    }
  }

  "getLastNTransaction should return error" when {
    "invoked with invalid account" in {
      assertFailure(GetAccountTransactionsRequest("srooor24450-sdkss", "WRONGACC1", 1), INVALID_ACCOUNT_ERROR, getAccountInfoService.getLastNAllTransactions)
    }
    "invoked with nonexistent account" in {
      assertFailure(GetAccountTransactionsRequest("srooor24450-sdkss", "1234567890", 1), NO_ACCOUNT_ERROR, getAccountInfoService.getLastNAllTransactions)
    }
  }

  "getAccountInformation method return account information" when {
    "invoked with a valid existing account" in {
      getAccountInfoService.getAccountInformation(GetAccountInformationRequest("srooor24450-sdkss", "1111111111")) shouldBe Account("1111111111", 80000, USD, REGULAR, xCurrencyTransferEnabled = false).success
    }
  }

  "getBalance should return account balance" when {
    "invoked with a valid existing account" in {
      getAccountInfoService.getBalance(GetAccountInformationRequest("srooor24450-sdkss", "1111111111")) shouldBe BigDecimal("80000").success
    }
  }

  "getLastNAllTransaction should return 2 transaction" when {
    "invoked with a valid existing account having one transaction" in {
      getAccountInfoService.getLastNAllTransactions(GetAccountTransactionsRequest("srooor24450-sdkss", "1111111111", 2)) shouldBe
        Seq(
          Transaction("7tf00edd-csdt-fd87-d9ff-9sf999f09990", "1111111111", "3333333333", "1111111111", LocalDate.of(2018, 4, 3), 5000, 89000, 94000, USD, INR, CREDIT),
          Transaction("6d00ede2-c9cf-4d8c-a915-012a717155ef", "1111111111", "1111111111", "2222222222", LocalDate.of(2018, 4, 1), 1000, 90000, 89000, USD, INR, DEBIT)
        ).success
    }
  }

  "getLastNCreditTransaction should return 1 transaction" when {
    "invoked with a valid existing account having one transaction" in {
      getAccountInfoService.getLastNCreditTransactions(GetAccountTransactionsRequest("srooor24450-sdkss", "1111111111", 2)) shouldBe
        Seq(
          Transaction("7tf00edd-csdt-fd87-d9ff-9sf999f09990", "1111111111", "3333333333", "1111111111", LocalDate.of(2018, 4, 3), 5000, 89000, 94000, USD, INR, CREDIT)
        ).success
    }
  }

  "getLastNDebitTransaction should return 2 transaction" when {
    "invoked with a valid existing account having one transaction" in {
      getAccountInfoService.getLastNDebitTransactions(GetAccountTransactionsRequest("srooor24450-sdkss", "1111111111", 2)) shouldBe
        Seq(
          Transaction("6d00ede2-c9cf-4d8c-a915-012a717155ef", "1111111111", "1111111111", "2222222222", LocalDate.of(2018, 4, 1), 1000, 90000, 89000, USD, INR, DEBIT)
        ).success
    }
  }
}
