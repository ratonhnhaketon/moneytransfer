package com.mt.app.service

import com.mt.app.lock.DummyDistributedLock
import com.mt.app.model.enum.ErrorCode._
import com.mt.app.model.request.MoneyTransferRequest
import com.mt.app.store.{AccountStore, ExchangeRateStore, TransactionStore}
import org.junit.runner.RunWith
import org.scalatest.Matchers.convertToAnyShouldWrapper
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, WordSpec}

@RunWith(classOf[JUnitRunner])
class MoneyTransferServiceSpec extends WordSpec with BeforeAndAfter {

  private val accountStore = new AccountStore
  private val exchangeRateStore = new ExchangeRateStore
  private val transactionStore = new TransactionStore
  private val dummyLock = new DummyDistributedLock[String]
  private val moneyTransferService = new MoneyTransferServiceImpl(accountStore, exchangeRateStore, transactionStore, dummyLock, 0.01, "8844221100")

  after(dummyLock.clear())

  "Money Transfer request should fail if a one transfer request is already being processed" in {
    dummyLock.tryLock("1111111111")
    dummyLock.isLockAcquired("1111111111") shouldBe true
    assertFailure(MoneyTransferRequest("sdsdds89977-sddaxc", "1111111111", "2222222222", 800, Option("Rent Transfer")), TRANSACTION_IN_PROGRESS_ERROR, moneyTransferService.transfer)
  }

  "Money Transfer request should fail if a one of the accounts is missing" in {
    assertFailure(MoneyTransferRequest("sdsdds89977-sddaxc", "2111111111", "2222222222", 800, Option("Rent Transfer")), NO_SOURCE_ACCOUNT_ERROR, moneyTransferService.transfer)
    assertFailure(MoneyTransferRequest("sdsdds89977-sddaxc", "1111111111", "1222222222", 800, Option("Rent Transfer")), NO_TARGET_ACCOUNT_ERROR, moneyTransferService.transfer)
  }

  "Money Transfer request should fail when source account doesn't have sufficient balance" in {
    assertFailure(MoneyTransferRequest("sdsdds89977-sddaxc", "5555555555", "8888888888", 100000, Option("Rent Transfer")), INSUFFICIENT_BALANCE_ERROR, moneyTransferService.transfer)
  }

  "Money Transfer request should fail if source and target currencies are different and xCurrencyTransfer is disabled" in {
    assertFailure(MoneyTransferRequest("sdsdds89977-sddaxc", "1111111111", "2222222222", 800, Option("Rent Transfer")), XCURR_TRANSFER_ERROR, moneyTransferService.transfer)
  }

  "Money Transfer request should fail if invalid amount is used" in {
    assertFailure(MoneyTransferRequest("sdsdds89977-sddaxc", "1111111111", "2222222222", -1, Option("Rent Transfer")), INVALID_TRANSFER_AMOUNT_ERROR, moneyTransferService.transfer)
  }

  "Money Transfer request should fail if invalid account value is used" in {
    assertFailure(MoneyTransferRequest("sdsdds89977-sddaxc", "1111111111111", "2222222222", -1, Option("Rent Transfer")), INVALID_ACCOUNT_ERROR, moneyTransferService.transfer)
  }

  "Money Transfer request should be processed if source and target currencies are same and xCurrencyTransfer is disabled" in {
    accountStore.getAccountInformation("1111111111").map(_.balance shouldBe BigDecimal("80000"))
    accountStore.getAccountInformation("6666666666").map(_.balance shouldBe BigDecimal("700000"))

    val result = moneyTransferService.transfer(MoneyTransferRequest("sdsdds89977-sddaxc", "1111111111", "6666666666", 15000, Option("Rent Transfer")))

    result.isSuccess shouldBe true
    accountStore.getAccountInformation("1111111111").map(_.balance shouldBe BigDecimal("65000"))
    accountStore.getAccountInformation("6666666666").map(_.balance shouldBe BigDecimal("715000"))

    transactionStore.getLastNAllTransactions("1111111111", 1).map(_.amount shouldBe 15000)
    transactionStore.getLastNAllTransactions("6666666666", 1).map(_.amount shouldBe 15000)
  }

  "Money Transfer request should be processed with amount updated with MarkUp if markup flag is enable" in {
    accountStore.getAccountInformation("2222222222").map(_.balance shouldBe BigDecimal("3500000"))
    accountStore.getAccountInformation("4444444444").map(_.balance shouldBe BigDecimal("90000"))

    val result = moneyTransferService.transfer(MoneyTransferRequest("sdsdds89977-sddaxc", "2222222222", "4444444444", 1000000, Option("Car Payment")))

    result.isSuccess shouldBe true
    accountStore.getAccountInformation("2222222222").map(_.balance shouldBe BigDecimal("2490000"))
    accountStore.getAccountInformation("4444444444").map(_.balance shouldBe BigDecimal("1090000"))

    transactionStore.getLastNAllTransactions("2222222222", 2).exists(_.amount == 1000000) shouldBe true
    transactionStore.getLastNAllTransactions("2222222222", 2).exists(_.amount == 10000) shouldBe true
    transactionStore.getLastNAllTransactions("4444444444", 1).map(_.amount shouldBe 1000000)
  }

  "Money Transfer request should be processed when source and target currencies are different and xCurrencyTransfer is enabled" in {
    accountStore.getAccountInformation("7777777777").map(_.balance shouldBe BigDecimal("500000"))
    accountStore.getAccountInformation("8888888888").map(_.balance shouldBe BigDecimal("1000000"))

    val result = moneyTransferService.transfer(MoneyTransferRequest("sdsdds89977-sddaxc", "7777777777", "8888888888", 100000, Option("Hotel Payment")))

    result.isSuccess shouldBe true
    accountStore.getAccountInformation("7777777777").map(_.balance shouldBe BigDecimal("358000"))
    accountStore.getAccountInformation("8888888888").map(_.balance shouldBe BigDecimal("1100000"))

    transactionStore.getLastNAllTransactions("7777777777", 1).map(_.amount shouldBe 142000)
    transactionStore.getLastNAllTransactions("8888888888", 1).map(_.amount shouldBe 100000)
  }
}
