package com.mt.app.service

import com.mt.app.lock.DummyDistributedLock
import com.mt.app.model.enum.AccountType._
import com.mt.app.model.enum.ErrorCode.{INVALID_ACCOUNT_ERROR, NO_ACCOUNT_ERROR}
import com.mt.app.model.request.{AccountFeatureEnableRequest, AccountTypeUpdateRequest}
import com.mt.app.store.AccountStore
import org.junit.runner.RunWith
import org.scalatest.Matchers.convertToAnyShouldWrapper
import org.scalatest.WordSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class UpdateAccountInformationServiceSpec extends WordSpec {

  private val accountStore = new AccountStore
  private val distributedLock = new DummyDistributedLock[String]
  private val updateAccountInformationService = new UpdateAccountInformationServiceImpl(accountStore, distributedLock)

  "updateXCurrencyTransfer method should return error" when {
    "invoked with invalid account" in {
      assertFailure(AccountFeatureEnableRequest("assdj444-99fui", "WRONGACC1", isFeatureEnable = true), INVALID_ACCOUNT_ERROR, updateAccountInformationService.updateXCurrencyTransfer)
    }
    "invoked with nonexistent account" in {
      assertFailure(AccountFeatureEnableRequest("assdj444-99fui", "1234567890", isFeatureEnable = true), NO_ACCOUNT_ERROR, updateAccountInformationService.updateXCurrencyTransfer)
    }
  }
  "updateXCurrencyTransfer method should update xxCurrencyTransfer flag" when {
    "invoked with valid account" in {
      updateAccountInformationService.updateXCurrencyTransfer(AccountFeatureEnableRequest("assdj444-99fui", "1111111111", isFeatureEnable = true))
        .isSuccess shouldBe true
    }


    "updateAccountType method shoud return error" when {
      "invoked with invalid account" in {
        assertFailure(AccountTypeUpdateRequest("assdj444-99fui", "WRONGACC1", PREMIUM), INVALID_ACCOUNT_ERROR, updateAccountInformationService.updateAccountType)
      }
      "invoked with nonexistent account" in {
        assertFailure(AccountTypeUpdateRequest("assdj444-99fui", "1234567890", PREMIUM), NO_ACCOUNT_ERROR, updateAccountInformationService.updateAccountType)
      }
    }
    "updateAccountType method should update accountType value" when {
      "invoked with valid account" in {
        updateAccountInformationService.updateAccountType(AccountTypeUpdateRequest("assdj444-99fui", "1111111111", PREMIUM))
          .isSuccess shouldBe true
      }
    }

    "updateAccountState method should return error" when {
      "invoked with invalid account" in {
        assertFailure(AccountFeatureEnableRequest("assdj444-99fui", "WRONGACC1", isFeatureEnable = false), INVALID_ACCOUNT_ERROR, updateAccountInformationService.updateAccountState)
      }
      "invoked with nonexistent account" in {
        assertFailure(AccountFeatureEnableRequest("assdj444-99fui", "1234567890", isFeatureEnable = false), NO_ACCOUNT_ERROR, updateAccountInformationService.updateAccountState)
      }
    }
    "updateAccountState method should update accountState value" when {
      "invoked with valid account" in {
        updateAccountInformationService.updateAccountState(AccountFeatureEnableRequest("assdj444-99fui", "1111111111", isFeatureEnable = true))
          .isSuccess shouldBe true
      }
    }

    "updateAccountMarkUp method should return error" when {
      "invoked with invalid account" in {
        assertFailure(AccountFeatureEnableRequest("assdj444-99fui", "WRONGACC1", isFeatureEnable = false), INVALID_ACCOUNT_ERROR, updateAccountInformationService.updateAccountMarkUp)
      }
      "invoked with nonexistent account" in {
        assertFailure(AccountFeatureEnableRequest("assdj444-99fui", "1234567890", isFeatureEnable = false), NO_ACCOUNT_ERROR, updateAccountInformationService.updateAccountMarkUp)
      }
    }
    "updateAccountMarkUp method should update markUp flag" when {
      "invoked with valid account" in {
        updateAccountInformationService.updateAccountMarkUp(AccountFeatureEnableRequest("assdj444-99fui", "1111111111", isFeatureEnable = true))
          .isSuccess shouldBe true
      }
    }
  }
}

