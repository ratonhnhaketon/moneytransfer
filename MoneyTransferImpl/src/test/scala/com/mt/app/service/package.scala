package com.mt.app

import com.mt.app.model.enum.ErrorCode.ErrorCode
import org.scalatest.Matchers.convertToAnyShouldWrapper

import scala.util.Try

package object service {

  /**
    * Generic Method to Assert Failures
    * @param request
    * @param errorCode
    * @param f
    * @tparam T
    * @tparam R
    */
  def assertFailure[T, R](request: R, errorCode: ErrorCode, f: R => Try[T]): Unit = {
    val result = f(request)
    result.isFailure shouldBe true
    result.failed.map(ex => ex.toString shouldBe errorCode.toString)
  }
}
